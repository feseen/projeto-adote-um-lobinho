async function getWolves(adopted){
    let wolves = []
    const options = {method: 'GET'};
    
    let url = 'https://lobinhos.herokuapp.com/wolves'

    if(adopted) url += '/adopted'

    const response = await fetch(url, options)
                            .catch(err => console.log(err));
    const data = await response.json();

    await data.forEach(wolf => wolves.push(wolf))

    return wolves

}

function loadWolfPage(wolf){
    console.log(wolf)
    let wolfId = Number(wolf.id)
    
    localStorage.setItem('id', wolfId)

    location.href = "../html-pages/show-wolf.html"
}

function createWolfDiv(wolf, isLeft){

    let isAdopted = (wolf.adopted) ? "adopted" : "to-adopt"
    let adoptedButton = (wolf.adopted) ? "Adotado" : "Adotar"

    const leftDiv = 
    `<div class="wolf left" id="${wolf.id}">
        <div class="wolf-portrait">
            <img class="wolf-img" src=${wolf.image_url}>
            <div class="wolf-img-shadow"></div>
        </div>
        <div class="wolf-info">
            <div class="wolf-name-adopt">
                <div class="wolf-name-age">
                    <p class="wolf-name">${wolf.name}</p>
                    <p class="wolf-age">Idade: ${wolf.age} anos</p>
                </div>
                <button class="adopt ${isAdopted}">${adoptedButton}</button>
            </div>
            <div class="wolf-desc">
                <p>${wolf.description}</p>
            </div>
        </div>   
    </div>`

    const rightDiv = 
    `<div class="wolf right" id="${wolf.id}">
        <div class="wolf-info">
            <div class="wolf-name-adopt">
                <button class="adopt ${isAdopted}">${adoptedButton}</button>
                <div class="wolf-name-age">
                    <p class="wolf-name">${wolf.name}</p>
                    <p class="wolf-age">Idade: ${wolf.age} anos</p>
                </div>
            </div>
        <div class="wolf-desc">
            <p>${wolf.description}</p>
        </div>
    </div>   
        <div class="wolf-portrait">
            <img class="wolf-img" src=${wolf.image_url}>
            <div class="wolf-img-shadow"></div>
        </div>
    </div>`

    let div = (isLeft) ? leftDiv : rightDiv

    document.getElementById("wolves").innerHTML += div
}

function filterWolves(wolf, input){
    if(wolf.name.toLowerCase().includes(input.toLowerCase())){
        document.getElementById(wolf.id).style.display = "flex"
    }
    else{
        document.getElementById(wolf.id).style.display = "none"
        console.log(document.getElementById(wolf.id))
    }
}

document.getElementById("search").addEventListener("keyup", (searchBar) => {
    wolves.then(wolves => wolves.forEach(wolf => filterWolves(wolf, searchBar.target.value)))
})


document.addEventListener("click", (div) => {
    if(div.target.getAttribute('class') == "adopt to-adopt"){
        loadWolfPage(div.target.parentElement.parentElement.parentElement)
    }
    else if(div.target.getAttribute('class') == "adopt adopted"){
        window.alert("Lobinho já adotado.")
    }
})

document.getElementById("add").addEventListener("click", () => location.href = "../html-pages/add-wolf.html")

document.getElementById("see-adopted-only").addEventListener("change", (check) => {
    if(check.target.checked){
        wolves = getWolves(1)
    } 
    else{
        wolves = getWolves(0)
    }
    wolves.then(wolves => {
        isLeft = 0
        document.getElementById("wolves").innerHTML = ""
        wolves.forEach(wolf => createWolfDiv(wolf, isLeft = !isLeft))
    })
})

var wolves = getWolves(0)
let isLeft = 0
let currentPage = 1

wolves.then(wolves => wolves.forEach(wolf => {
    createWolfDiv(wolf, isLeft = !isLeft)
}))