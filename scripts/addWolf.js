function validateInput(inputName, inputAge, inputUrl, inputDesc){
    if(inputName.length < 4 || inputName.length > 60){
        window.alert("Nome Inválido.")
        return 0
    }
    if(inputAge < 0 || inputAge > 100){
        window.alert("Idade Inválida.")
        return 0
    }
    if(inputUrl == ""){
        window.alert("Por favor, insira uma imagem.")
        return 0
    }
    if(inputDesc == ""){
        window.alert("Por favor, insira uma descrição.")
        return 0
    }
    else if(inputDesc.length < 10){
        window.alert("Descrição muito curta.")
        return 0
    }
    else if(inputDesc.length > 255){
        window.alert("Descrição muito longa.")
        return 0
    }
    console.log(inputName, inputAge, inputUrl, inputDesc)
    return 1
}


document.querySelector("#wolf-add-submit").addEventListener("click", () => {
    let inputName = document.querySelector("#wolf-name").value
    let inputAge = document.querySelector("#wolf-age").value
    let inputUrl = document.querySelector("#wolf-photo-url").value
    let inputDesc = document.querySelector("#wolf-description").value


    if(validateInput(inputName, inputAge, inputUrl, inputDesc)){
        let newWolf = {
            "wolf": {
                name: inputName, 
                age: inputAge,
                image_url: inputUrl,
                description: inputDesc
            }
        }
    
        const options = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(newWolf)
          };
          
          fetch('https://lobinhos.herokuapp.com/wolves/', options)
            .then(response => response.json())
            .then(window.alert("Lobinho adicionado com sucesso. S2"))
            .catch(err => window.alert("Algo deu errado. Por favor, tente novamente."));
    }
})
