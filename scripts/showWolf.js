const id = localStorage.getItem('id')

async function getWolf(id){
    let wolf = {}

    const options = {method: 'GET'};

await fetch('https://lobinhos.herokuapp.com/wolves/' + id, options)
        .then(response => response.json())
        .then(response => {wolf = Object.assign(wolf, response)})
        .catch(err => console.error(err));

    return wolf
}

function deleteWolf(id){
    const options = {method: 'DELETE'};

    fetch('https://lobinhos.herokuapp.com/wolves/' + id, options)
        .then(response => response.json())
        .then(response => console.log(response))
        .catch(err => console.error(err));
}

function displayWolf(wolf){
    document.querySelector("title").innerHTML = wolf.name

    document.getElementById("wolf-name").innerHTML = wolf.name
    document.getElementById("wolf-image").setAttribute("src", wolf.image_url)
    document.getElementById("description").innerHTML = wolf.description
}

const wolf = getWolf(id)
wolf.then(wolf => displayWolf(wolf))

document.getElementById("delete").addEventListener("click", () => {
    deleteWolf(id)
})

document.getElementById("adopt").addEventListener("click", () => {
    location.href = "../html-pages/adopt-wolf.html"
})