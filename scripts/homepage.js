let random = (length) => Math.floor(Math.random() * length)

function createWolfDiv(wolf, isLeft){
    console.log(wolf)
    const leftDiv = 
    `<div class="wolf left">
        <div class="wolf-portrait">
            <img class="wolf-img" src=${wolf.image_url}>
            <div class="wolf-img-shadow"></div>
        </div>
        <div class="wolf-info">
            <div class="wolf-name-age">
                <p class="wolf-name">${wolf.name}</p>
                <p class="wolf-age">Idade: ${wolf.age} anos</p>
            </div>
            <div class="wolf-desc">
                <p>${wolf.description}</p>
            </div>
        </div>   
    </div>`

    const rightDiv = 
    `<div class="wolf right">
        <div class="wolf-info">
            <div class="wolf-name-age">
                <p class="wolf-name">${wolf.name}</p>
                <p class="wolf-age">Idade: ${wolf.age} anos</p>
            </div>
            <div class="wolf-desc">
                <p>${wolf.description}</p>
            </div>
        </div>   
        <div class="wolf-portrait">
            <img class="wolf-img" src=${wolf.image_url}>
            <div class="wolf-img-shadow"></div>
        </div>
    </div>`

    let div = (isLeft) ? leftDiv : rightDiv

    document.getElementById("wolves-example").innerHTML += div
}

async function getWolves(){
    let wolves = []
    const options = {method: 'GET'};

    await fetch('https://lobinhos.herokuapp.com/wolves/', options)
        .then(response => response.json())
        .then(response => {response.forEach(wolf => wolves.push(wolf))})
        .catch(err => console.error(err));

    return wolves
}

let wolves = getWolves()
wolves.then(wolves => createWolfDiv(wolves[random(wolves.length)], true))
wolves.then(wolves => createWolfDiv(wolves[random(wolves.length)], false))