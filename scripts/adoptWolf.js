let id = localStorage.getItem('id')

async function getWolf(id){

    let wolf = {}

    const options = {method: 'GET'};

await fetch('https://lobinhos.herokuapp.com/wolves/' + id, options)
        .then(response => response.json())
        .then(response => {wolf = Object.assign(wolf, response)})
        .catch(err => console.error(err));

    return wolf
}

function displayWolf(wolf){
    document.querySelector("title").innerHTML = "Adote o(a) " + wolf.name

    document.getElementById("wolf-name").innerHTML = wolf.name
    document.getElementById("wolf-photo").setAttribute("src", wolf.image_url)
    document.getElementById("wolf-id").innerHTML += wolf.id
}

function getAdopterData(adopter){
    let name = document.getElementById("owner-name").value
    let age = document.getElementById("owner-age").value
    let email = document.getElementById("owner-email").value

    if(name == ""){
        window.alert("Nome Inválido.")
        return 0
    }
    if(age <= 0){
        window.alert("Idade Inválida.")
        return 0
    }
    if(email == ""){
        window.alert("E-mail Inválido.")
        return 0
    }
    adopter.adopter_name = name
    adopter.adopter_age = age
    adopter.adopter_email = email
    return 1
}

function putAdopter(id){

    const temp = { 
        wolf:{
            adopter_name: "",
            adopter_age: 0,
            adopter_email: ""
            } 
        }

    if(!getAdopterData(temp.wolf)){
        return
    }

    const options = {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(temp)
      };
      
      fetch('https://lobinhos.herokuapp.com/wolves/' + id, options)
        .then(response => response.json())
        .then(window.alert("Validação concluída. Adoção bem-sucedida."))
        .catch(err => window.alert(err));
    }

document.getElementById("wolf-adopt-submit").addEventListener("click", () => {
    putAdopter(id)
})

const wolf = getWolf(id)
wolf.then(wolf => {
    if(wolf.adopted){
        location.href = "../html-pages/homepage.html"
    }
    else{
        displayWolf(wolf)
    }   
})